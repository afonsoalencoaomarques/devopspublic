# Class Assignment 3 - Part 1

#### We start by creating our VM as described in the lecture.

# GIT issue:

Everything was done according to the lecture, but when I first tried installing GIT with:

```
sudo apt install git
```

My GIT installation failed to fetch some archives, showing:

```
E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing?
```

When I tried:

```
apt-get update
```

It still showed some problems:

```
E: Could not open lock file /var/lib/apt/lists/lock - open (13: Permission denied)
E: Unable to lock directory /var/lib/apt/lists/
W: Problem unlinking the file /var/cache/apt/pkgcache.bin - RemoveCaches (13: Permission denied)
W: Problem unlinking the file /var/cache/apt/srcpkgcache.bin - RemoveCaches (13: Permission denied)
```

But running the same command as superuser seemed to fix the problem:

```
sudo apt-get update 
```

# SSH Server, Port 22 issue:

The first time I connected to the VM with the user name and IP of the VM:

```
Ssh afonsoalencoao@192.168.56.100
```

I was successful. But when I closed the host terminal and the VM, the following attempt at connecting to the VM through the host, it showed me:

```
ssh: connect to host 192.168.56.100 port 22: Connection refused
```

I tried many solutions I found by searching:

- Checking if the ssh is active, restarting it, stopping and starting it.
- Checking port 22 to see if it was listening.
- Checking if the Firewall was not blocking anything relevant to this issue.

But nothing seemed to fix it. The first time I was successful in reconnecting was by restarting everything in the VM and host terminal. But, once again, when I later tried to connect again to the VM, I found the same issue.

The definite solution to this problem was actually simple. I just changed the VM's IP from:

```
192.168.56.100
```

to:

```
192.168.56.5
```

This seemed to have fixed the problem. 
The first IP used is the default IP used by the DHCP server on VirtualBox, and it may have been causing conflict.

Everything else went according to the lecture.

### Cloning projects 

1. We now clone our projects from previous assignments, by cloning our individual repository:

```
git clone https://afonsoalencoaomarques@bitbucket.org/afonsoalencoaomarques/devops-19-20-a-1191742.git
```

We can and should install all the necessary dependencies on the VM, such as maven, or gradle:

```
sudo apt install [what you want to install]
```

## CA1

On our VM, go to the 'basic' folder of our CA1:

```
cd /devops-19-20-a-1191742/CA1/basic
```

Run the project:

```
./mvnw spring-boot:run
```

If everything was done and is working correctly, if we go to an web browser in our host machine (since our VM does not support a graphic interface) and go to:

```
192.168.56.5:8080
```

It should show us the table we created when we did this CA.

## CA2

On our VM and in our host machine, go to the folder:

```
cd devops-19-20-a-1191742/CA2/Part1
```

Edit the file *build.gradle* and change the 'localhost' address (in the runClient task) to the same IP of our VM, in this case: **192.168.56.5**

In our VM, Run the project as a server with the command:

````
./gradlew runserver
````

In our host machine, we run the project as a client with the command:

```
./gradlew runclient
```

If everything is working, we should now have our simple chat application running.

# Why is this required?

The client needs a graphic window, something that the VM does not have. So, it's necessary to use two different machines, with the same IP and credentials to connect to each other.


