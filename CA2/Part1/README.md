Gradle Basic Demo
===================

This is a demo application that implements a basic multithreaded chat room server.

The server supports several simultaneous clients through multithreading. When a client connects the server requests a screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.


Prerequisites
-------------

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 6.6 (if you do not use the gradle wrapper in the project)
   

Build
-----

To build a .jar file with the application:

    % ./gradlew build 

Run the server
--------------

Open a terminal and execute the following command from the project's root directory:

    % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>

Substitute <server port> by a valid por number, e.g. 59001

Run a client
------------

Open another terminal and execute the following gradle task from the project's root directory:

    % ./gradlew runClient

The above task assumes the chat server's IP is "localhost" and its port is "59001". If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

To run several clients, you just need to open more terminals and repeat the invocation of the runClient gradle task


Class Assignment 2
------------

I decided to change the port to "8141", for test purposes. If you whish to use other parameters please edit the runClient task in the "build.gradle" file in the project's root directory.

Add Unit Test
------------

I decided to do the unit tests before the task.

To add unit tests to the project, a new dependency must be added to the `build.gradle` file:

```groovy
dependencies {
    //other dependencies
testCompile group: 'junit', name: 'junit', version: '4.12'
}
```

After this, create a new test class and add a simple unit test:

```java
package basic_demo;

import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {

    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}
```

Run the server
------------

```groovy
task runServer(type:JavaExec, dependsOn: classes){
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '8141'
}
```

To run the tests using gradle, open a terminal and simply write: 

```console
./gradlew test
```

Add a Copy Task
------------

To copy the contents of a folder to another, a new task of type **Copy** can be created in the `build.gradle` file.

```groovy
task copyFiles(type: Copy){
    from('src')
    into('backup')
}
```
In this case, the contents of the `src` folder are going to be copied to a new `backup` folder. You can run this task with:

```console
./gradlew CopyFiles
```

Add a Zip Task
------------
To copy the contents of the `src` to a new **zip** file, add the following task:

```groovy
task zipFiles(type: Zip){
    from('src')
    archiveName 'zipFile.zip'
    destinationDir(file('backup'))
}
```
A zip file with the contents of the `src` will be created in the `backup` folder if you run this in the terminal:

```console
./gradlew zipFiles
```






