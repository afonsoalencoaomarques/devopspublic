package com.greglturnquist.payroll.employee;

import com.greglturnquist.payroll.Employee;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeTest {

    @Test
    @DisplayName("Test the Employee constructor")
    public void createEmployee() {

        //Arrange
        String firstName = "Son";
        String lastName = "Goku";
        String description = "Saiyajin";
        String jobTitle = "Gatherer of the Balls";
        String email = "kakarot@vegeta.com";

        //Act
        Employee empSayan = new Employee(firstName, lastName, description, jobTitle, email);

        //Assert
        assertEquals(firstName, empSayan.getFirstName());
        assertEquals(lastName, empSayan.getLastName());
        assertEquals(description, empSayan.getDescription());
        assertEquals(jobTitle, empSayan.getJobName());
        assertEquals(email, empSayan.getEmailField());
    }

    @Test
    @DisplayName("Validate EmailField")
    public void validateEmailField() {
        boolean validateEmail = false;
        //Arrange
        String firstName = "Son";
        String lastName = "Goku";
        String description = "Saiyajin";
        String jobTitle = "Gatherer of the Balls";
        String email = "thisIsNotAnEmail";

        //Act
        Employee empSayan = new Employee(firstName, lastName, description, jobTitle, email);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Employee>> violations = validator.validate(empSayan);

        //Assert
        for (ConstraintViolation<Employee> x : violations) {
            if (x.getPropertyPath().iterator().next().getName().equals("emailField")) {
                validateEmail = true;
            }
        }
        assertTrue(validateEmail);
    }

    @Test
    @DisplayName("Validate FirstName")
    public void validateFirstName() {
        boolean validate = false;
        //Arrange
        String firstName = null;
        String lastName = "Goku";
        String description = "Saiyajin";
        String jobTitle = "Gatherer of the Balls";
        String email = "kakarot@vegeta.com";

        //Act
        Employee empSayan = new Employee(firstName, lastName, description, jobTitle, email);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Employee>> violations = validator.validate(empSayan);

        //Assert
        for (ConstraintViolation<Employee> x : violations) {
            if (x.getPropertyPath().iterator().next().getName().equals("firstName")) {
                validate = true;
            }
        }
        assertTrue(validate);
    }

    @Test
    @DisplayName("Validate lastName")
    public void validateLastName() {
        boolean validate = false;
        //Arrange
        String firstName = "Son";
        String lastName = null;
        String description = "Saiyajin";
        String jobTitle = "Gatherer of the Balls";
        String email = "kakarot@vegeta.com";

        //Act
        Employee empSayan = new Employee(firstName, lastName, description, jobTitle, email);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Employee>> violations = validator.validate(empSayan);

        //Assert
        for (ConstraintViolation<Employee> x : violations) {
            if (x.getPropertyPath().iterator().next().getName().equals("lastName")) {
                validate = true;
            }
        }
        assertTrue(validate);
    }

    @Test
    @DisplayName("Validate description")
    public void validateDescription() {
        boolean validate = false;
        //Arrange
        String firstName = "Son";
        String lastName = "Goku";
        String description = null;
        String jobTitle = "Gatherer of the Balls";
        String email = "kakarot@vegeta.com";

        //Act
        Employee empSayan = new Employee(firstName, lastName, description, jobTitle, email);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Employee>> violations = validator.validate(empSayan);

        //Assert
        for (ConstraintViolation<Employee> x : violations) {
            if (x.getPropertyPath().iterator().next().getName().equals("description")) {
                validate = true;
            }
        }
        assertTrue(validate);
    }

    @Test
    @DisplayName("Validate jobTitle")
    public void validatejobTitle() {
        boolean validate = false;
        //Arrange
        String firstName = "Son";
        String lastName = "Goku";
        String description = "Saiyajin";
        String jobTitle = null;
        String email = "kakarot@vegeta.com";

        //Act
        Employee empSayan = new Employee(firstName, lastName, description, jobTitle, email);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Employee>> violations = validator.validate(empSayan);

        //Assert
        for (ConstraintViolation<Employee> x : violations) {
            if (x.getPropertyPath().iterator().next().getName().equals("jobName")) {
                validate = true;
            }
        }
        assertTrue(validate);
    }
}