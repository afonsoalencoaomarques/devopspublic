# CA1 - Report

### Tag the initial version as v1.2.0

I've decided to tag the initial version in my "update gitignore to avoid Mac Files" commit, because my other commits were just made to understand the workings of the console, git, and bitbucket. 
I run the following command on the console:

```console
$ git tag -a v1.2.0 -m "Initial Version for CA1"
```

Afterwards, we need to update it in the repository:

```console
$ git push origin v1.2.0
```

### Create branch "email-field" and work on the code and tests

To create and checkout a new local branch, we run the following command:

```console
$ git checkout -b email-field
```

### After developing the code and tests

Now we should commit the work:

add:
```console
$ add
```

```console
$ git commit -m "ref #2 - Add support for email-field"
```

And push:

```
$ git push -u origin email-field
```

Now to merge the **email-field** branch with the master branch:
Begin to checkout to master:

```console
$ git checkout master
```
Merge:

```console
$ git merge email-field
```

Push:

```console
$ git push
```

I want to Tag, so:

```console
$ git tag -a v1.3.0 -m "For now"
```

Push the tag:

```console
$ git push origin v1.3.0
```







